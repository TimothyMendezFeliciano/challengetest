package Rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Obtain {
	
	String url  = "http://34.201.19.114:40003/recordController/getAllRecords";
	URL obj;
	BufferedReader read;
	HttpURLConnection connection;
	String input;
	StringBuffer response = new StringBuffer();
	int counter = 0;
	ArrayList<Integer> latitudeIndex = new ArrayList<>();
	int end = response.lastIndexOf("latitude");
	ArrayList<String> latitude = new ArrayList<>();
	
	
	public void readData() throws IOException {
		obj = new URL(url);
		connection = (HttpURLConnection) obj.openConnection();
		connection.setRequestMethod("GET");
		read = new BufferedReader( new InputStreamReader(connection.getInputStream()));

		while(( input = read.readLine()) != null ) {
			response.append(input);
		}
		read.close();

		while(counter < end) {
			int temp = response.indexOf("latitude", counter);
			latitudeIndex.add(temp);
			latitude.add(response.substring(temp, temp+17));
			counter = temp+1;
		}
		System.out.println(latitudeIndex);
		System.out.println(latitude);
	}

}
